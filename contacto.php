
<?
	$whost = "https://".$_SERVER['HTTP_HOST'];
	$whost.="/";
	#$whost.="/web_vidal/";
	include_once('modules/module_process.php');
	include_once('modules/module_views.php');
	include_once('modules/module_loaders.php');
	$objprocess= new process();
	$objviews= new views($whost);
	$objloaders= new loaders($whost);
?><!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Google Tag Manager-->
    <script>
      (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-N74WMXW');
    </script>
    <!-- End Google Tag Manager-->
    <title>Textiles V&amp;V</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><? $objloaders->fonts() ; ?><? $objloaders->links() ; ?><? $objloaders->scripts() ; ?>
  </head>
  <body>
    <main>
      <header>
        <div class="header-top-side"></div>
        <div class="header-middle-side">
          <figure class="header-logo"><? $objloaders->logo() ; ?></figure>
          <ul class="header-icon">
            <li><a href="mailto:informes@textilesvyv.com" title="Escríbenos"><i class="fa fa-envelope-o" aria-hidden="true"></i>
                <div class="icon-data"><span>Email</span>
                  <label>informes@textilesvyv.com</label>
                </div></a></li>
            <li><a href="tel:01 354 9002" title="Consultas"><i class="fa fa-phone" aria-hidden="true"></i>
                <div class="icon-data"><span>Central Telefónica</span>
                  <label>01 354 9002</label>
                </div></a></li>
            <li><a href="https://www.facebook.com/textilesvyv/" target="_blank" title="Encuéntranos en Facebook!"><i class="fa fa-facebook" area-hidden="true"></i>
                <div class="icon-data"></div></a></li>
          </ul>
        </div>
        <div class="header-bottom-side">
          <div class="header-bar"><a class="fa fa-bars" href="#" aria-hidden="true"></a></div>
          <nav class="header-navigator">
            <ul><? $objloaders->menu_navigator(); ?></ul>
          </nav>
        </div>
      </header>
      <div class="content">
        <div class="content-contacto">
          <ul class="contacto-panel">
            <li><a href="#layer-contactus">Contáctanos</a></li>
            <li><a class="selected" href="#layer-findus">Encuéntranos</a></li>
          </ul>
          <div class="contacto-layer">
            <div class="layer-contactus" id="layer-contactus">
              <form method="post" id="frm-contact" name="frm-contact">
                <div class="contact-row">
                  <div class="row-label">
                    <label>Nombre y Apellido</label>
                  </div>
                  <div class="row-field">
                    <input type="text" name="txt_names" id="txt_names" value="" required>
                  </div>
                </div>
                <div class="contact-row">
                  <div class="row-label">
                    <label>Teléfono de Contacto</label>
                  </div>
                  <div class="row-field">
                    <input type="text" name="txt_phone" id="txt_phone" value="" required>
                  </div>
                </div>
                <div class="contact-row">
                  <div class="row-label">
                    <label>Dirección electrónica</label>
                  </div>
                  <div class="row-field">
                    <input type="email" name="txt_email" id="txt_email" value="" required>
                  </div>
                </div>
                <div class="contact-row">
                  <div class="row-label">
                    <label>Consulta</label>
                  </div>
                  <div class="row-field">
                    <textarea></textarea>
                  </div>
                </div>
                <div class="contact-row">
                  <input type="submit" name="bt_process" id="bt_process" value="Enviar Solicitud">
                </div>
              </form>
            </div>
            <div class="layer-findus selected" id="layer-findus">
              <div class="findus-address">
                <h1>Dirección</h1>
                <div class="infotext">
                  <label>Asoc. Sol de las Vinas, Cal. las Cucardas Mz C Lt 10 - Ate.</label>
                </div>
                <div class="infotext">
                  <label>Refe: 11.5 Km Carretera central.</label>
                </div>
              </div>
              <div class="findus-details">
                <h1>Datos de Contacto</h1>
                <div class="infotext">
                  <label>Telefono : (01) 354 - 9002 </label>
                </div>
                <div class="infotext">
                  <label>Whatsapp : +51 987246396 / +51 924462186</label>
                </div>
                <div class="infotext">
                  <label>Correos :  informes@textilesvyv.com</label>
                </div>
              </div>
              <div class="findus-location">
                <h1>Datos de Localización</h1>
                <div id="map-contact">
                  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPZOqZegUaJho8H_Gq-fQ4GJqTQ1aqIGE&amp;callback=loadMap"></script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer>
        <div class="footer-menu">
          <ul><? $objloaders->menu_navigator(); ?></ul>
        </div>
        <div class="footer-links">
          <div class="links-item" title="Escríbenos"><a class="fa fa-envelope-o" href="mailto:informes@textilesvyv.com" title="Escríbenos"></a>
          </div>
          <div class="links-item" title="Consultas"><a class="fa fa-phone" href="tel:013549002" title="Consultas"></a>
          </div>
          <div class="links-item" title="Siguenos en Facebook!"><a class="fa fa-facebook" href="https://www.facebook.com/textilesvyv/" target="_blank" title="Siguenos en Facebook!"></a>
          </div>
          <div class="links-item" title="Encuéntranos"><a class="fa fa-map-marker" href="https://www.google.com/maps/?q=-12.013994,-76.879820" target="_blank" title="Encuéntranos"></a>
          </div>
        </div>
        <div class="footer-copyright">
          <label>Copyright 2018 - Todos los derechos reservados - <a href='http://www.textilesvyv.com' target='_blank' >textilesvyv.com</a></label>
        </div>
      </footer>
      <div class="wallpaper"></div>
    </main>
    <!-- Google Tag Manager (noscript)-->
    <noscript>
      <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N74WMXW" height="0" width="0" style="display:none;visibility:hidden;"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript)-->
  </body>
</html>