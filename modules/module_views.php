<?
if(!class_exists('views')){
	class views {
		var $wroot='';
		function __construct($wpath='http://localhost/'){
			$this->wroot = $wpath; 
		}
		public function f_display_error($wmessage){
            ?> 
            <div class="message-noresult">
                            <h3>
                            <i class="fa fa-meh-o" aria-hidden="true"></i>
                            <?= $wmessage ?>
                    </h3>
            </div>
            <?
        }
		public function f_display_header_content($wlist,$wtype='vertical-view'){
			switch($wtype){
				case 'gallery-services':
						if(count($wlist)>0){
							$wnitem=0;
							foreach($wlist as $witem){
								if($witem['content_level']=='PARENT'){
									$wnitem+=1; 
									?>
									<div class="content-row" id="crow-item-<?= $wnitem ?>">
										<? if($witem['content_image']!=''){
												$wpath="img/".$witem['content_image'];
												if(file_exists($wpath)){ ?>
												  <figure class="row-picture">
														<img src="<?= $this->wroot.utf8_encode($wpath) ?>" />
												  </figure>													
											 <? }
										   }  ?>
										   <h2 class="row-title"><?=utf8_encode($witem['content_value']) ?></h2> 
									</div> <?
								}
							}
						}
					break;
				case 'gallery-about':
					if(count($wlist)>0){
						$wnitem=0;
						foreach($wlist as $witem){
							if($witem['content_level']=='PARENT'){
								$wnitem=$wnitem+1;
								$wdirection=(($wnitem % 2)==0?'right':'left'); 
								?>
								<div class="content-row <?=$wdirection ?>" id="crow-item-<?= $wnitem ?>">
										<? if($witem['content_image']!=''){?>
												<div class="row-icon"><i class="<?= $witem['content_image'] ?>" aria-hidden="true"></i></div>
										<? } ?>
										<div class="row-body">
											<h2 class="row-title"><?=utf8_encode($witem['content_value']) ?></h2> <?
											foreach($wlist as $wchild){
												if($wchild['content_level']=='CHILD' && $wchild['content_order']==$witem['content_order']){
													?><p class="row-paragraph"><?=utf8_encode($wchild['content_value']) ?></p><?			
												}	
											} ?>
										</div>
								</div>
						    <?} 
						}
					}
					break;
				case 'vertical-view':
						if(count($wlist)>0){
							foreach($wlist as $witem){
								if($witem['content_level']=='PARENT'){ ?>
									<div class="content-row">
										<h2 class="row-title"><?=utf8_encode($witem['content_value']) ?></h2> <?
										foreach($wlist as $wchild){
											if($wchild['content_level']=='CHILD' && $wchild['content_order']==$witem['content_order']){
												?><p class="row-paragraph"><?=utf8_encode($wchild['content_value']) ?></p><?			
											}	
										} ?>
									</div>
						    	<? }
							}
						}					
					break;
				case 'gallery-view':
						if(count($wlist)>0){
							foreach($wlist as $witem){
								if($witem['content_level']=='PARENT'){ ?>
									<div class="content-row">
										<h2 class="row-title"><?=utf8_encode($witem['content_value']) ?></h2> 
										<?
											if($witem['content_image']!=''){
												$wpath='img/'.$witem['content_image'];
												if(file_exists($wpath)){ ?>
												  <figure class="row-picture">
														<a>
															<img src="<?= utf8_encode($wpath) ?>" />
															<span>
																<label class="text"><?=utf8_encode($witem['content_value'])?></label>
															</span>
														</a>
												  </figure>													
											 <? }
											} 
										?>
									</div> <?
								}
							}
						}
					break;
			}
		}
	}
	$obj_views= new views();	
}
?>