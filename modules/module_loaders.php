<?
	if(!class_exists("loaders")){
		class loaders {
			var $wroot='';
			function __construct($wpath='http://localhost/'){
				$this->wroot = $wpath; 
			}
			private function print_modules($witems){
				foreach($witems as $witem) {
					switch($witem['type']){
						case 'css':
								?> <link href="<?=$witem['module']?>" type="text/css" rel="stylesheet" > <?
							break;
						case 'javascript':
								?> <script src="<?=$witem['module']?>" type="text/javascript"></script> <?
							break;
						case 'icon':
								?> <link href="<?=$witem['module']?>" type="image/x-icon" rel="shortcut icon"><?	
							break;
						case 'short-icon':
								?> <link href="<?=$witem['module']?>" type="image/x-icon" rel="icon"> <?
							break;
					}
				}
			}
			public function fonts(){
				$witems = array(
							array( "module"=>$this->wroot."css/font-awesome.css","type"=>"css"),
							array( "module"=>$this->wroot."img/favicon.ico","type"=>"icon"),
							array( "module"=>$this->wroot."img/favicon.ico","type"=>"short-icon")
						);
				$this->print_modules($witems);
				return ;
			}
			public function links(){
				$witems = array(
							array( "module"=>$this->wroot."css/normalize.css","type"=>"css"),
							array( "module"=>$this->wroot."css/owl.carousel.min.css","type"=>"css"),
							array( "module"=>$this->wroot."css/owl.theme.default.min.css","type"=>"css"),
							array( "module"=>$this->wroot."css/stylesheets/main.css.css","type"=>"css")
							// array( "module"=>$this->wroot."js/jquery-ui-1.12.1/jquery-ui.theme.min.css","type"=>"css"),
							// array( "module"=>$this->wroot."js/jquery-ui-1.12.1/jquery-ui.min.css","type"=>"css"),
						);
				$this->print_modules($witems);
				return ;
			}
			public function scripts(){
				$witems = array( 
								array( "module"=>$this->wroot."js/modernizr.js","type"=>"javascript"),
								array( "module"=>$this->wroot."js/jquery-3.2.1.min.js","type"=>"javascript"),
								// array( "module"=>$this->wroot."js/jquery-ui-1.12.1/jquery-ui.min.js","type"=>"javascript"),
								array( "module"=>$this->wroot."js/owl.carousel.min.js","type"=>"javascript"),
								array( "module"=>$this->wroot."js/main.js","type"=>"javascript")
							);
				$this->print_modules($witems);
				return ;
			}
			public function menu_navigator(){
				$wlabels= array('Inicio','Nosotros','Productos','Servicios','Contacto');
				$wsource= array('home','nosotros','productos','servicios','contacto');
				$witems = array(count($wlabels));
				for($nitem=0;$nitem<=count($wlabels)-1;$nitem++){
					$witems[$nitem] = array('label'=> $wlabels[$nitem],'source'=>$wsource[$nitem]);
				}
				if(count($witems)>0){
					foreach($witems as $witem){?>
						<li><a href="<?= $this->wroot.$witem['source'] ;?>"><?=$witem['label'] ;?></a></li>
					<?}
				}
			}
			public function menu_slider(){
				$wpath='img/';
				$wimgs = array('img_flyer_1','img_flyer_2','img_flyer_3','img_flyer_4');
				foreach($wimgs as $wimg){
					$wsource = $this->wroot.$wpath.$wimg.".jpg";
					?><div class="item" style="background-size:cover; background-image: url(<?=$wsource ?>) ;"></div><?
				}
			}
			public function logo(){
				$wsource="{$this->wroot}img/img_logo.png";
				$whome="{$this->wroot}home";
				?> <a href="<?=$whome;?>">
						<img src="<?=$wsource;?>">
				   </a><?
			}
			
		}
	}
	$obj_loaders = new loaders();
?>