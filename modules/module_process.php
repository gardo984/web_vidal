<?
	include_once('module_connection.php');

	if(!class_exists('process')){
		class process {
			private function f_sanitize_parameter($wvalue,$wtype){
                $wstatus = true;
                switch($wtype){
                        case 'number':
                                if(preg_match("/^[0-9]+$/",$wvalue)==false){
                                        $wstatus=false; 
                                }
                           break;
                        default:
                                $wstatus=false;
                           break;
                }
                return $wstatus;
            }
			public function f_get_header_content($wheader,$wcontent_type='1'){
				global $obj_connection;
				$wcmd="call sp_get_header_content('get_headers','$wheader',0,'$obj_connection->company')";
				$wlist= $obj_connection->f_get_array_from($obj_connection->f_get_query($wcmd));
				return $wlist;
			}
			public function f_get_header_id($wrequest){
                global $obj_connection;
                $wcmd="call sp_get_header_id('$wrequest','$obj_connection->company')";
                $wlist= $obj_connection->f_get_array_from($obj_connection->f_get_query($wcmd));
                $obj_connection->f_get_free_results($obj_connection->db);
                return $wlist;
            }
			public function f_get_request_file($wrequest){
                $wpage='';
                if(strlen($wrequest)>0){
                        # Validating is there's querystring or not
                        if(preg_match("/\?/",$wrequest)==true){
                                $wsplit = explode('?',$wrequest);
                                $wrequest = $wsplit[0];
                        }
                        $wlist= explode('/',$wrequest);
                        $wpage= $wlist[count($wlist)-1];        
                }
                return $wpage;
            }

			
		}
		$obj_process= new process();
	}
?>
