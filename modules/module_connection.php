<?
	if(!class_exists('connection')){
		class connection {
			var $db;
			var $company;
			function __construct(){
				$this->company='';
				$wusername='';
				$wpasswd='';
				$whost='';
				$wdatabase='';
				$this->db = @mysqli_connect($whost,$wusername,$wpasswd,$wdatabase);
				if(!$this->db){
					die(mysqli_connect_error($this->db));
				}
				return ;
			}
			public function f_get_query($wsentence){
				$wresult = mysqli_query($this->db,$wsentence) or die(mysqli_error($this->db));
				return $wresult;
			}
			public function f_get_free_results($wconnection){
				while(mysqli_next_result($wconnection)){
					if($wresult= mysqli_store_result($wconnection)){
						$wresult->free();
					}
				}
				return ;
			}
			public function f_get_array_from($wresult){
				$wlist= array();
				while($witem = mysqli_fetch_array($wresult)){
					$wlist[]= $witem;
				}
				return $wlist;
			}
			public function f_get_columns_from($wresult){
				$wfields = mysqli_fetch_fields($wresult);
				$wlist=array();
				$wnitem=0;
				foreach($wfields as $wfield){
					$wlist[$wnitem] = $wfield->name;
					$wnitem = $wnitem + 1;
				}
				return $wlist;
			}
		}
		$obj_connection= new connection();
	}
?>
