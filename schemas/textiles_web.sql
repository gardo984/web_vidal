-- MySQL dump 10.16  Distrib 10.1.13-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: textilesvyv.com    Database: textiles_website
-- ------------------------------------------------------
-- Server version	5.5.59-cll

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `web_companies`
--

DROP TABLE IF EXISTS `web_companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_companies` (
  `company_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_name` char(100) DEFAULT NULL,
  `dtmodify` datetime DEFAULT NULL,
  `company_status` enum('ENABLE','DISABLE') DEFAULT 'ENABLE',
  PRIMARY KEY (`company_id`),
  KEY `dtregister` (`dtregister`) USING BTREE,
  KEY `company_status` (`company_status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_companies`
--

LOCK TABLES `web_companies` WRITE;
/*!40000 ALTER TABLE `web_companies` DISABLE KEYS */;
INSERT INTO `web_companies` VALUES (1,'2017-11-18 17:04:12','V & V Textiles',NULL,'ENABLE');
/*!40000 ALTER TABLE `web_companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `web_content_type`
--

DROP TABLE IF EXISTS `web_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_content_type` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type_description` varchar(100) DEFAULT NULL,
  `type_status` enum('ENABLE','DISABLE') DEFAULT 'ENABLE',
  PRIMARY KEY (`type_id`),
  KEY `dtregister` (`dtregister`) USING BTREE,
  KEY `type_status` (`type_status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_content_type`
--

LOCK TABLES `web_content_type` WRITE;
/*!40000 ALTER TABLE `web_content_type` DISABLE KEYS */;
INSERT INTO `web_content_type` VALUES (1,'2017-12-04 02:11:51','CONTENT','ENABLE'),(2,'2017-12-04 02:12:03','EVENTS','ENABLE');
/*!40000 ALTER TABLE `web_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `web_eventos`
--

DROP TABLE IF EXISTS `web_eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_eventos` (
  `event_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_description` varchar(100) DEFAULT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `event_status` enum('ENABLE','DISABLE') DEFAULT 'ENABLE',
  `event_type` enum('UNIQUE','RANGE') DEFAULT 'UNIQUE',
  `event_from` datetime DEFAULT NULL,
  `event_to` datetime DEFAULT NULL,
  `event_time_from` time DEFAULT NULL,
  `event_time_to` time DEFAULT NULL,
  `event_content` longtext,
  `event_image` varchar(100) DEFAULT NULL,
  `event_campaign` varchar(50) DEFAULT NULL,
  `company_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`event_id`),
  KEY `dtregister` (`dtregister`) USING BTREE,
  KEY `event_status` (`event_status`) USING BTREE,
  KEY `event_description` (`event_description`) USING BTREE,
  KEY `event_from` (`event_from`) USING BTREE,
  KEY `event_to` (`event_to`) USING BTREE,
  KEY `event_time_from` (`event_time_from`) USING BTREE,
  KEY `event_time_to` (`event_time_to`) USING BTREE,
  KEY `event_type` (`event_type`) USING BTREE,
  KEY `event_campaign` (`event_campaign`) USING BTREE,
  KEY `company_id` (`company_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_eventos`
--

LOCK TABLES `web_eventos` WRITE;
/*!40000 ALTER TABLE `web_eventos` DISABLE KEYS */;
INSERT INTO `web_eventos` VALUES (1,'Charla orientativa sobre estudios de grado en España','2017-12-04 01:58:07','ENABLE','UNIQUE','2017-12-14 00:00:00','2017-12-14 00:00:00','16:00:00','16:00:00',NULL,'uploads/img_charla.jpg','CULTURA',2),(2,'Lluís Bonet presenta La gestión de teatros: modelos y estrategias para equipamientos culturales','2017-12-04 01:59:25','ENABLE','UNIQUE','2017-12-12 00:00:00','2017-12-12 00:00:00','19:30:00','19:30:00',NULL,'uploads/img_lbonet.jpg','CULTURA',2),(3,'Los Secretos. Una vida a tu lado\r\n','2017-12-04 01:58:07','ENABLE','UNIQUE','2017-12-05 00:00:00','2017-12-05 00:00:00','11:00:00','11:00:00',NULL,'uploads/img_charla.jpg','CULTURA',2),(4,'Lluís Bonet presenta La gestión de teatros: modelos y estrategias para equipamientos culturales','2017-12-04 01:59:25','ENABLE','UNIQUE','2017-12-01 00:00:00','2017-12-01 00:00:00','19:30:00','19:30:00',NULL,'uploads/img_lbonet.jpg','CULTURA',2),(5,'Convocatoria del XIX Concurso Internacional de Piano de Santander','2017-12-04 01:58:07','ENABLE','UNIQUE','2017-12-29 00:00:00','2017-12-29 00:00:00','19:00:00','19:00:00',NULL,'uploads/img_charla.jpg','CULTURA',2),(6,'Lluís Bonet presenta La gestión de teatros: modelos y estrategias para equipamientos culturales','2017-12-04 01:59:25','ENABLE','UNIQUE','2017-12-06 00:00:00','2017-12-06 00:00:00','19:30:00','19:30:00',NULL,'uploads/img_lbonet.jpg','CULTURA',2),(7,'Andrés Suárez, Desde una Ventana Tour','2017-12-04 01:58:07','ENABLE','UNIQUE','2017-12-14 00:00:00','2017-12-14 00:00:00','16:00:00','16:00:00',NULL,'uploads/img_charla.jpg','CULTURA',2),(8,'VIII Festival Internacional de Guitarra','2017-12-04 01:59:25','ENABLE','UNIQUE','2017-12-06 00:00:00','2017-12-06 00:00:00','19:30:00','19:30:00',NULL,'uploads/img_lbonet.jpg','CULTURA',2),(9,'Los Protones en La Fiesta de la Música','2017-12-04 01:58:07','ENABLE','UNIQUE','2017-12-12 00:00:00','2017-12-12 00:00:00','13:00:00','13:00:00',NULL,'uploads/img_charla.jpg','CULTURA',2),(10,'Javier Perianes en concierto','2017-12-04 01:59:25','ENABLE','UNIQUE','2017-12-05 00:00:00','2017-12-05 00:00:00','14:30:00','14:30:00',NULL,'uploads/img_lbonet.jpg','CULTURA',2),(11,'Posibilidades dramatúrgicas contemporáneas de textos narrativos y clásicos','2017-12-07 02:36:41','ENABLE','UNIQUE','2017-12-08 00:00:00','2017-12-08 00:00:00','11:00:00','11:00:00',NULL,'uploads/img_lbonet.jpg','CULTURA',2);
/*!40000 ALTER TABLE `web_eventos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `web_headers`
--

DROP TABLE IF EXISTS `web_headers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_headers` (
  `header_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `header_item` int(10) unsigned DEFAULT NULL,
  `header_description` char(100) DEFAULT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `header_status` enum('ENABLE','DISABLE') DEFAULT 'ENABLE',
  `header_lang` enum('spanish','english') DEFAULT 'spanish',
  `company_id` int(10) unsigned NOT NULL,
  `header_request` char(25) DEFAULT NULL,
  PRIMARY KEY (`header_id`),
  KEY `header_item` (`header_item`) USING BTREE,
  KEY `dtregister` (`dtregister`) USING BTREE,
  KEY `header_status` (`header_status`) USING BTREE,
  KEY `company_id` (`company_id`) USING BTREE,
  KEY `header_request` (`header_request`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_headers`
--

LOCK TABLES `web_headers` WRITE;
/*!40000 ALTER TABLE `web_headers` DISABLE KEYS */;
INSERT INTO `web_headers` VALUES (1,1,'NOSOTROS','2017-10-31 23:56:42','ENABLE','spanish',1,'nosotros'),(2,2,'SERVICIOS','2017-10-31 23:56:44','ENABLE','spanish',1,'servicios'),(3,3,'CONTACTO','2017-10-31 23:56:45','ENABLE','spanish',1,'contacto'),(9,4,'PRODUCTOS','2017-12-11 04:17:15','ENABLE','spanish',1,'productos');
/*!40000 ALTER TABLE `web_headers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `web_headers_content`
--

DROP TABLE IF EXISTS `web_headers_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_headers_content` (
  `content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `header_id` int(10) unsigned NOT NULL,
  `content_value` longtext,
  `content_level` enum('PARENT','CHILD') DEFAULT NULL,
  `content_parent` int(10) unsigned DEFAULT NULL,
  `content_item` int(10) unsigned DEFAULT NULL,
  `content_status` enum('ENABLE','DISABLE') DEFAULT 'ENABLE',
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content_image` char(100) DEFAULT NULL,
  `content_video` char(100) DEFAULT NULL,
  `content_type` int(10) unsigned NOT NULL,
  PRIMARY KEY (`content_id`),
  KEY `header_id` (`header_id`) USING BTREE,
  KEY `content_level` (`content_level`) USING BTREE,
  KEY `content_parent` (`content_parent`) USING BTREE,
  KEY `content_status` (`content_status`) USING BTREE,
  KEY `dtregister` (`dtregister`) USING BTREE,
  KEY `content_type` (`content_type`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_headers_content`
--

LOCK TABLES `web_headers_content` WRITE;
/*!40000 ALTER TABLE `web_headers_content` DISABLE KEYS */;
INSERT INTO `web_headers_content` VALUES (1,1,'HISTORIA','PARENT',NULL,1,'ENABLE','2017-11-01 00:12:40','fa fa-globe',NULL,1),(2,1,'SOMOS','PARENT',NULL,2,'ENABLE','2017-11-01 00:12:57','fa fa-users',NULL,1),(3,1,'VISION','PARENT',NULL,3,'ENABLE','2017-11-01 00:13:00','fa fa-graduation-cap',NULL,1),(4,1,'MISION','PARENT',NULL,4,'ENABLE','2017-11-01 00:13:01','fa fa-plane',NULL,1),(5,1,'OBJETIVOS','PARENT',NULL,5,'ENABLE','2017-11-01 00:13:17','fa fa-briefcase',NULL,1),(6,1,'VALORES','PARENT',NULL,6,'ENABLE','2017-11-01 00:13:18','fa fa-thumbs-up',NULL,1),(8,1,'Textiles V&amp;V es una empresa familiar fundada en el año 2013 por su representante\r\nlegal Edward Vera Vidal. Dando inicio en las actividades de compra y venta de\r\nprendas de vestir para hombres.</br></br>\r\nEn el año 2014, se dio el gran salto al ingresar al Emporio Comercial de Gamarra,\r\nespecíficamente en la <b>\"Galeria Azul\"</b>, dedicándose a la venta al por mayor y menor de\r\nprendas de vestir para hombres.</br></br>\r\nEn el año 2015, se habilitó un taller de confección textil y estampado. La empresa dio\r\nun giro al segmento que se dedicada y comenzó con la confección de ropa industrial\r\npara empresas y a brindar servicios textiles.</br></br>\r\nEn la actualidad se encuentran ubicados en el distrito de Ate vitarte, dedicada al 100%\r\na los servicios textiles, publicitarios y confección de ropa industrial.','CHILD',1,1,'ENABLE','2017-11-01 00:15:14',NULL,NULL,1),(9,1,'Una empresa textil dedicada a la confección de prendas de vestir, ropa industrial y\r\nservicios textiles, nos enfocamos en brindar la más alta calidad en nuestros servicios.','CHILD',2,1,'ENABLE','2017-11-01 00:15:53',NULL,NULL,1),(12,1,'Estamos comprometidos a diseñar y producir productos de alta calidad de acuerdo a la\r\nnecesidad del mercado. Nuestro fin es lograr la satisfacción de nuestros clientes y\r\ncontribuir con el desarrollo económico del país capacitando y gestionando talento\r\nhumano.','CHILD',4,1,'ENABLE','2017-11-01 00:17:33',NULL,NULL,1),(11,1,'Ser una empresa líder en el sector textil caracterizado por brindar productos y\r\nservicios de alta calidad.','CHILD',3,1,'ENABLE','2017-11-01 00:16:15',NULL,NULL,1),(13,1,'Ampliar nuestro mercado a nivel nacional e internacional.<br>Mejora continua de la calidad.','CHILD',5,1,'ENABLE','2017-11-01 00:18:49',NULL,NULL,1),(14,1,'<b>Responsabilidad</b>: con el cliente, entregar a tiempo lo solicitado.<br><b>Respeto</b>: con nuestro cliente, proveedor al momento de brindar un servicio y con nuestros empleados generando un buen clima laboral.<br><b>Honestidad</b>: informar adecuadamente los insumos y la calidad del producto.','CHILD',6,1,'ENABLE','2017-11-01 00:19:17',NULL,NULL,1),(15,2,'Corte y confeccion','PARENT',NULL,1,'ENABLE','2017-11-01 01:01:43','img_servicios_1.jpg',NULL,1),(16,2,'Uniformes industriales','PARENT',NULL,2,'ENABLE','2017-11-01 01:02:33','img_servicios_2.jpg',NULL,1),(17,2,'Estampado','PARENT',NULL,3,'ENABLE','2017-11-01 01:02:42','img_servicios_3.jpg',NULL,1),(18,2,'Servicios publicitarios','PARENT',NULL,4,'ENABLE','2017-11-01 01:02:51','img_servicios_4.jpg',NULL,1),(19,2,'Chalecos de seguridad.','CHILD',16,1,'DISABLE','2017-11-01 01:04:24',NULL,NULL,1),(20,2,'jeans clasicos','CHILD',16,2,'DISABLE','2017-11-01 01:04:24',NULL,NULL,1),(21,2,'uniforme drill tecnologia.','CHILD',16,3,'DISABLE','2017-11-01 01:04:24',NULL,NULL,1),(22,2,'Buzos y casacas.','CHILD',16,4,'DISABLE','2017-11-01 01:04:24',NULL,NULL,1),(23,2,'Chompas.','CHILD',16,5,'DISABLE','2017-11-01 01:04:24',NULL,NULL,1),(24,2,'Mamelucos.','CHILD',16,6,'DISABLE','2017-11-01 01:04:24',NULL,NULL,1),(25,2,'Gorros legionarios','CHILD',16,7,'DISABLE','2017-11-01 01:04:24',NULL,NULL,1),(26,2,'Blusas y camisas.','CHILD',16,8,'DISABLE','2017-11-01 01:04:24',NULL,NULL,1),(27,2,'Zapatillas. ','CHILD',16,9,'DISABLE','2017-11-01 01:04:24',NULL,NULL,1),(28,2,'etc.','CHILD',16,10,'DISABLE','2017-11-01 01:04:24',NULL,NULL,1),(29,2,'Al agua.','CHILD',17,1,'DISABLE','2017-11-01 01:05:07',NULL,NULL,1),(30,2,'sublimado.','CHILD',17,2,'DISABLE','2017-11-01 01:05:07',NULL,NULL,1),(31,2,'plastisol.','CHILD',17,3,'DISABLE','2017-11-01 01:05:07',NULL,NULL,1),(32,2,'flock.','CHILD',17,4,'DISABLE','2017-11-01 01:05:07',NULL,NULL,1),(33,2,'Caviar. ','CHILD',17,5,'DISABLE','2017-11-01 01:05:07',NULL,NULL,1),(34,2,'Escarcha.','CHILD',17,6,'DISABLE','2017-11-01 01:05:07',NULL,NULL,1),(35,2,'3D','CHILD',17,7,'DISABLE','2017-11-01 01:05:07',NULL,NULL,1),(36,2,'Dicharge','CHILD',17,8,'DISABLE','2017-11-01 01:05:07',NULL,NULL,1),(37,2,'Tacto cero. ','CHILD',17,9,'DISABLE','2017-11-01 01:05:07',NULL,NULL,1),(38,2,'etc.','CHILD',17,10,'DISABLE','2017-11-01 01:05:07',NULL,NULL,1),(39,2,'Gorros.','CHILD',18,1,'DISABLE','2017-11-01 01:06:00',NULL,NULL,1),(40,2,'Polos publicitarios.','CHILD',18,2,'DISABLE','2017-11-01 01:06:00',NULL,NULL,1),(41,2,'bolsas y recuerdos de cumpleanos.','CHILD',18,3,'DISABLE','2017-11-01 01:06:00',NULL,NULL,1),(42,2,'etc.','CHILD',18,4,'DISABLE','2017-11-01 01:06:00',NULL,NULL,1),(94,9,'Polos','PARENT',NULL,1,'ENABLE','2017-12-13 16:13:33','img_products_polos.png',NULL,1),(95,9,'Chompas','PARENT',NULL,2,'ENABLE','2017-12-13 16:13:33','img_products_chompas.png',NULL,1),(96,9,'Chalecos','PARENT',NULL,3,'ENABLE','2017-12-13 16:13:33','img_products_chalecos.png',NULL,1),(97,9,'Camisas y Blusas','PARENT',NULL,4,'ENABLE','2017-12-13 16:13:35','img_products_camisas.png',NULL,1),(98,9,'Chaquetas','PARENT',NULL,5,'ENABLE','2017-12-13 16:14:07','img_products_chaqueta.png',NULL,1),(99,9,'Casacas','PARENT',NULL,6,'ENABLE','2017-12-13 16:14:10','img_products_casaca2.png',NULL,1),(100,9,'Gorros','PARENT',NULL,7,'ENABLE','2017-12-13 16:14:14','img_products_gorros.png',NULL,1),(101,9,'Pantalones','PARENT',NULL,8,'ENABLE','2017-12-13 16:14:18','img_products_pantalones.png',NULL,1),(102,9,'Mamelucos','PARENT',NULL,9,'ENABLE','2017-12-13 16:14:21','img_products_mameluco2.png',NULL,1),(103,9,'Mandiles','PARENT',NULL,10,'ENABLE','2017-12-13 16:14:25','img_products_mandiles.png',NULL,1),(104,9,'Articulos de Seguridad','PARENT',NULL,11,'ENABLE','2017-12-13 16:14:31','img_products_uniformes.png',NULL,1);
/*!40000 ALTER TABLE `web_headers_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `web_lists`
--

DROP TABLE IF EXISTS `web_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_lists` (
  `list_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` char(10) DEFAULT NULL,
  `description` char(200) DEFAULT NULL,
  `list_pattern` char(100) DEFAULT NULL,
  `list_status` enum('ENABLE','DISABLE') DEFAULT 'ENABLE',
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`list_code`),
  KEY `dtregister` (`dtregister`) USING BTREE,
  KEY `list_pattern` (`list_pattern`) USING BTREE,
  KEY `list_status` (`list_status`) USING BTREE,
  KEY `id` (`id`) USING BTREE,
  KEY `description` (`description`) USING BTREE,
  KEY `company_id` (`company_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_lists`
--

LOCK TABLES `web_lists` WRITE;
/*!40000 ALTER TABLE `web_lists` DISABLE KEYS */;
INSERT INTO `web_lists` VALUES (1,NULL,'YOGA','EVENT_CAMPAIGN','ENABLE','2017-12-04 03:28:47',2),(2,NULL,'COCINA','EVENT_CAMPAIGN','ENABLE','2017-12-04 03:28:59',2),(3,NULL,'VAISNAVA','EVENT_CAMPAIGN','ENABLE','2017-12-04 03:29:00',2),(4,NULL,'CULTURA','EVENT_CAMPAIGN','ENABLE','2017-12-04 03:29:00',2);
/*!40000 ALTER TABLE `web_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `web_months`
--

DROP TABLE IF EXISTS `web_months`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_months` (
  `code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month_id` char(2) DEFAULT NULL,
  `month_spanish` char(50) DEFAULT NULL,
  `month_english` char(50) DEFAULT NULL,
  PRIMARY KEY (`code`),
  KEY `month_id` (`month_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_months`
--

LOCK TABLES `web_months` WRITE;
/*!40000 ALTER TABLE `web_months` DISABLE KEYS */;
INSERT INTO `web_months` VALUES (1,'1','enero','january'),(2,'2','febrero','february'),(3,'3','marzo','march'),(4,'4','abril','april'),(5,'5','mayo','may'),(6,'6','junio','june'),(7,'7','julio','july'),(8,'8','agosto','august'),(9,'9','septiembre','september'),(10,'10','octubre','october'),(11,'11','noviembre','november'),(12,'12','diciembre','december');
/*!40000 ALTER TABLE `web_months` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `web_requests`
--

DROP TABLE IF EXISTS `web_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_requests` (
  `request_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nom_user` char(50) DEFAULT NULL,
  `host_server` char(50) DEFAULT NULL,
  `host_client` char(20) DEFAULT NULL,
  `host_client_port` int(8) unsigned DEFAULT NULL,
  `host_cookie` char(100) DEFAULT NULL,
  `navegator` char(200) DEFAULT NULL,
  `request_uri` longtext,
  `request_time` int(10) unsigned DEFAULT NULL,
  `request_method` char(20) DEFAULT NULL,
  `request_referer` longtext,
  `application_id` enum('WEBPAGE','INTRANET') DEFAULT NULL,
  `company_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`request_id`),
  KEY `fhlogin` (`dtregister`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_requests`
--

LOCK TABLES `web_requests` WRITE;
/*!40000 ALTER TABLE `web_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `web_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'textiles_website'
--
/*!50003 DROP FUNCTION IF EXISTS `f_get_monthname` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE  FUNCTION `f_get_monthname`( p_month int unsigned) RETURNS varchar(50) CHARSET utf8
begin
				select m.month_spanish into @message  
				from web_months  as m
				where m.month_id = p_month ;
				return @message;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_header_content` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_get_header_content`(wtype char(25),wheader_id int unsigned,wchild_id int unsigned,  wcompany_id int unsigned)
begin
				  case(wtype)
									when 'get_headers' then 
																select concat(if(w.content_level in(1),w.content_id,w.content_parent),'-',ifnull(if(w.content_level in(1),w.content_parent,w.content_item),0)) as content_hash,
																					if(w.content_level in(1),w.content_item, (select content_item from web_headers_content where content_id= w.content_parent limit 1)) as content_order,
																					if(w.content_level in(1),0,w.content_item) as child_order, w.content_id,w.header_id, w.content_value, w.content_level, w.content_status,
																					w.content_image, w.content_video
																from web_headers_content as w
																			inner join web_headers as h on h.header_id=w.header_id and h.header_status='ENABLE' 
																where w.content_status in(1) and  h.header_id = wheader_id
																						and h.company_id = wcompany_id 
																						and w.content_type in (1)
																order by content_order, child_order ;
								when 'get_headers_item' then 
															select concat(if(w.content_level in(1),w.content_id,w.content_parent),'-',ifnull(if(w.content_level in(1),w.content_parent,w.content_item),0)) as content_hash,
																					if(w.content_level in(1),w.content_item, (select content_item from web_headers_content where content_id= w.content_parent limit 1)) as content_order,
																					if(w.content_level in(1),0,w.content_item) as child_order, w.content_id,w.header_id, w.content_value, w.content_level, w.content_status,
																					w.content_image, w.content_video
																from web_headers_content as w
																				inner join web_headers as h on h.header_id=w.header_id and h.header_status='ENABLE' 
																where w.content_status in(1) and  h.header_id = wheader_id
																				and h.company_id = wcompany_id	
																				and w.content_type in (1)
																having substring_index(content_hash,'-',1) = wchild_id 
																order by content_order, child_order ;
							when 'get_events' then 
														select e.event_id, e.dtregister, e.event_description,  (case(e.event_type) 
																							when 'UNIQUE' then concat("Dia: ",day(e.event_from)," ",f_get_monthname(month(e.event_from))," ", year(e.event_from))
																			end ) as event_day,  (case(e.event_type) 
																							when 'UNIQUE' then concat(time_format(time(e.event_time_from),"%H:%i")," Horas") 
																			end ) as event_time , e.event_image
															from web_eventos as e 	
																				inner join web_headers_content as w on w.header_id = e.event_id and w.content_type = 2
															where e.company_id = wcompany_id 
																						and e.event_status = 'ENABLE'
															group by e.event_id
															order by e.dtregister desc ;
							when 'get_events_item' then 
												select z.*
												from (
															select e.event_id,'PARENT' as event_level, e.dtregister, e.event_description,  (case(e.event_type) 
																						when 'UNIQUE' then concat("Dia: ",day(e.event_from)," ",f_get_monthname(month(e.event_from))," ", year(e.event_from))
																		end ) as event_day,  (case(e.event_type) 
																						when 'UNIQUE' then concat(time_format(time(e.event_time_from),"%H:%i")," Horas") 
																		end ) as event_time , e.event_image
															from web_eventos as e 	
																		inner join web_headers_content  as w on w.header_id = e.event_id and w.content_type=2
															where e.company_id = 2 
																					and e.event_status = 'ENABLE'
															group by e.event_id
															union all 
															select  concat(w.header_id,'-',w.content_item) as header_id, 'CHILD' as event_level,
																					'' as dtregister,  w.content_value as event_description,'','',''
															from web_headers_content  as w 
																				inner join web_eventos  as e on w.header_id = e.event_id and w.content_type=2
															where w.content_type = 2
												) as  z
												where substring_index(z.event_id,'-',1)= wheader_id
												order by z.event_id asc ;
					end case;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_header_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE  PROCEDURE `sp_get_header_id`(wrequest char(25),wcompany_id int unsigned)
begin
						select  header_id from web_headers as h
						where h.company_id = wcompany_id  and h.header_status='ENABLE'
										and h.header_request regexp concat('^(',wrequest,')$') limit 1 ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-06 13:25:54
