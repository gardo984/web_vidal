
$(function(){
	var wi_wallpapers=false;
	// if element exists, call function
	if($('div.sliders').length) loadCarousel();
	
	// $('.hm-social i.fa.fa-search').on('click',f_search_appear);
	$('.header-bar a.fa.fa-bars').on('click',f_menu_appear);
	// function to attach all menu options to its source link
	f_attach_menu_links();
	// function to enable wallpapers translation
	//f_enable_wallpapers();
	// to call google maps and its right location
	switch(f_get_web_pathname()){
		case 'contacto':
				loadMap();
				f_attach_contact_layer();		
			break;
		default :
				f_scroll_event();	
			break;
	}
}) ;

function f_scroll_event() {
	var wparentPosition=$(this).scrollTop();
	var wparentSize=$(this).height();
	$(document).scroll(function(){
		var wparentPosition=$(this).scrollTop();
		var welements =$(document).find(".content-row");
		if(welements.lenght==0) return false;
		$.each(welements,function(windex,wele){
			var wtag = $(wele).attr('id');
			var wid = "#"+wtag;
			var wposition =$(wid).offset().top;
			if((wparentPosition+wparentSize) > wposition){
				$(wid).css({"opacity":"1"});
			}else{
				$(wid).css({"opacity":"0"});
			}
		});
	});
}
function f_attach_contact_layer(){
	var wele = $(".contacto-panel li").find("a");
	$.each(wele,function(windex,wele){
		$(wele).on('click',function(e){
			$(".contacto-panel li a.selected").each(function(windex,wele){
				$(wele).toggleClass('selected');
			});
			$(".contacto-layer > div.selected").each(function(windex,wele){
				$(wele).toggleClass('selected');
			});
			var whref=$(this).prop('href');
			var wid=whref.substring(whref.indexOf('#')+1);
			$(this).toggleClass('selected');
			$('#'+wid).toggleClass('selected');
			e.preventDefault();		
		});
	});
}
function f_enable_wallpapers() {
	var wimage = "img_wallpaper_gs1.jpg";
	$('.wallpaper').css({'background-image': 'url(img/'+ wimage +')'});
	
	// wi_wallpapers = setInterval(function(){
		// var witems=['img_wallpaper_gs1.jpg','img_wallpaper_gs2.jpg','img_wallpaper_gs3.jpg','img_wallpaper_gs4.jpg'];
		// var nitems=witems.length;
		// var wimage= Math.floor((Math.random() * nitems)+1);
		// if($('.wallpaper').length){
			// $('.wallpaper').css({'background-image': 'url(img/'+ witems[wimage-1]+')'});
		// }
	// },9000);
}

function f_attach_menu_links() {
	var wlist=$('.header-navigator ul li').find('a');
	var wpage=f_get_web_pathname();
	$.each(wlist,function(windex,wobj){
		var wsource= $(wobj).attr('href');
		wsource = wsource.substr(wsource.lastIndexOf('/')+1);
		if(wsource==wpage){
			$(wobj).toggleClass('selected');
			if(wpage!='home'){
				if($('.content').length){
					var wtag="<div class='banner-label'><a href='home' title='Inicio'>Inicio</a>/<label>"+
								$(wobj).html().toUpperCase()+"</label></div>";
					$('.content').prepend('<div class="content-banner">'+wtag+'</div>');
					return false;
				}
			}
		}
	});
}
function f_get_web_pathname(){
	var wpath=window.location.pathname;
	var wname=wpath.substring(wpath.lastIndexOf('/')+1);
	return wname;
}

function f_search_appear() {

}
function f_menu_appear(e) {
	
	// alert('u game me click :D');
	$('.header-bottom-side .header-navigator').toggleClass('appear');
	e.preventDefault();	
}
function loadCarousel(){
	var owl = $('div.sliders');
    owl.owlCarousel({
        loop:true,
	    autoplay:true,
	    autoplayTimeout: 2500,
	    // lazyLoad: true,
        autoplayHoverPause: false,
        smartSpeed:450,
        nav:true,
		navText: [ '<span class="nav-left-arrow"><<</span>', '<span class="nav-right-arrow">>></span>' ],
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:1,
	            nav:true
	        },
	        600:{
	            items:1,
	            nav:false
	        },
	        1000:{
	            items:1,
	            nav:true,
	        }
	    }
    });
}
function loadMap() {
	try {
		var uluru = {lat: -12.013994, lng: -76.879820};
		var map = new google.maps.Map(document.getElementById('map-contact'), {
		      zoom: 16,
		      center: uluru
		    });
	    var marker = new google.maps.Marker({
	      position: uluru,
	      map: map
	    });	
	} catch(err) {
		console.log("Error identified : " + err);
	}
	
}