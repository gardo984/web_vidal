
<?
	$whost = "https://".$_SERVER['HTTP_HOST'];
	$whost.="/";
	#$whost.="/web_vidal/";
	include_once('modules/module_process.php');
	include_once('modules/module_views.php');
	include_once('modules/module_loaders.php');
	$objprocess= new process();
	$objviews= new views($whost);
	$objloaders= new loaders($whost);
?><!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Google Tag Manager-->
    <script>
      (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-N74WMXW');
    </script>
    <!-- End Google Tag Manager-->
    <title>Textiles V&amp;V</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><? $objloaders->fonts() ; ?><? $objloaders->links() ; ?><? $objloaders->scripts() ; ?>
  </head>
  <body>
    <main>
      <header>
        <div class="header-top-side"></div>
        <div class="header-middle-side">
          <figure class="header-logo"><? $objloaders->logo() ; ?></figure>
          <ul class="header-icon">
            <li><a href="mailto:informes@textilesvyv.com" title="Escríbenos"><i class="fa fa-envelope-o" aria-hidden="true"></i>
                <div class="icon-data"><span>Email</span>
                  <label>informes@textilesvyv.com</label>
                </div></a></li>
            <li><a href="tel:01 354 9002" title="Consultas"><i class="fa fa-phone" aria-hidden="true"></i>
                <div class="icon-data"><span>Central Telefónica</span>
                  <label>01 354 9002</label>
                </div></a></li>
            <li><a href="https://www.facebook.com/textilesvyv/" target="_blank" title="Encuéntranos en Facebook!"><i class="fa fa-facebook" area-hidden="true"></i>
                <div class="icon-data"></div></a></li>
          </ul>
        </div>
        <div class="header-bottom-side">
          <div class="header-bar"><a class="fa fa-bars" href="#" aria-hidden="true"></a></div>
          <nav class="header-navigator">
            <ul><? $objloaders->menu_navigator(); ?></ul>
          </nav>
        </div>
      </header>
      <div class="content">
        <article class="content-builder">
          <?
                  include_once('modules/module_builder.php');
          ?>
        </article>
      </div>
      <footer>
        <div class="footer-menu">
          <ul><? $objloaders->menu_navigator(); ?></ul>
        </div>
        <div class="footer-links">
          <div class="links-item" title="Escríbenos"><a class="fa fa-envelope-o" href="mailto:informes@textilesvyv.com" title="Escríbenos"></a>
          </div>
          <div class="links-item" title="Consultas"><a class="fa fa-phone" href="tel:013549002" title="Consultas"></a>
          </div>
          <div class="links-item" title="Siguenos en Facebook!"><a class="fa fa-facebook" href="https://www.facebook.com/textilesvyv/" target="_blank" title="Siguenos en Facebook!"></a>
          </div>
          <div class="links-item" title="Encuéntranos"><a class="fa fa-map-marker" href="https://www.google.com/maps/?q=-12.013994,-76.879820" target="_blank" title="Encuéntranos"></a>
          </div>
        </div>
        <div class="footer-copyright">
          <label>Copyright 2018 - Todos los derechos reservados - <a href='http://www.textilesvyv.com' target='_blank' >textilesvyv.com</a></label>
        </div>
      </footer>
      <div class="wallpaper"></div>
    </main>
    <!-- Google Tag Manager (noscript)-->
    <noscript>
      <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N74WMXW" height="0" width="0" style="display:none;visibility:hidden;"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript)-->
  </body>
</html>